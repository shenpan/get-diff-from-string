package com.company;

import org.junit.Test;

import static org.junit.Assert.*;

public class MainTest {

    @Test
    public void getDifferencesInTwoStringsLowerCaseTest() {
        String oneString = "this is one string";
        String anotherString = "this is another string";
        String result = Main.getDifferences(oneString, anotherString);
        assertEquals("one || another",result);
    }

    @Test
    public void getDifferencesInTwoStringsUpperCaseTest() {
        String oneString = "THIS IS ONE STRING";
        String anotherString = "this is another string";
        String result = Main.getDifferences(oneString, anotherString);
        assertEquals("one || another",result);
    }

    @Test
    public void getDifferencesInTwoStringsLowerAndUpperTest() {
        String oneString = "THIS is One String";
        String anotherString = "this IS Another sTRING";
        String result = Main.getDifferences(oneString, anotherString);
        assertEquals("one || another",result);
    }

    @Test
    public void getDifferencesInTwoStringsDuplicatedStringsTest() {
        String oneString = "this is one string this is one string";
        String anotherString = "this is another string this is another string";
        String result = Main.getDifferences(oneString, anotherString);
        assertEquals("one || another",result);
    }

    @Test
    public void getDifferencesInTwoStringsSpecialCharactersTest() {
        String oneString = "this ~ is @# one $% <string> ^&";
        String anotherString = "this + - {is} [another]| /(string) *";
        String result = Main.getDifferences(oneString, anotherString);
        assertEquals("one || another",result);
    }

    @Test
    public void getDifferencesInTwoStringsPunctuationTest() {
        String oneString = "this's, one \"string\"!";
        String anotherString = "this's. another: string?";
        String result = Main.getDifferences(oneString, anotherString);
        assertEquals("one || another",result);
    }

    @Test
    public void getDifferencesInTwoStringsSpacesTest() {
        String oneString = "   this   is one   string      ";
        String anotherString = "    this  is   another   string ";
        String result = Main.getDifferences(oneString, anotherString);
        assertEquals("one || another",result);
    }

    @Test
    public void getDifferencesInTwoStringsOneEmptyValueTest() {
        String oneString = "";
        String anotherString = "this is another string";
        String result = Main.getDifferences(oneString, anotherString);
        assertEquals(" || this is another string",result);
    }

    @Test
    public void getDifferencesInTwoStringsAllEmptyValueTest() {
        String oneString = "";
        String anotherString = "";
        String result = Main.getDifferences(oneString, anotherString);
        assertEquals(" || ",result);
    }

    @Test
    public void getDifferencesInTwoStringsIntValuesTest() {
        String oneString = "this is 1 string";
        String anotherString = "this is 100 string";
        String result = Main.getDifferences(oneString, anotherString);
        assertEquals("1 || 100",result);
    }

    @Test
    public void getDifferencesInTwoStringsFloatValuesTest() {
        String oneString = "this is 1.01 string";
        String anotherString = "this is 100.01 string";
        String result = Main.getDifferences(oneString, anotherString);
        assertEquals("1 || 100",result);
    }

    @Test
    public void getDifferencesInTwoStringsExactMatchTest() {
        String oneString = "this is one string";
        String anotherString = "this is one string";
        String result = Main.getDifferences(oneString, anotherString);
        assertEquals(" || ",result);
    }

    @Test
    public void getDifferencesInTwoStringsTotalNotMatchTest() {
        String oneString = "this is one string";
        String anotherString = "totally different words here";
        String result = Main.getDifferences(oneString, anotherString);
        assertEquals("this is one string || totally different words here",result);
    }
}