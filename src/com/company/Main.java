package com.company;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    private static ArrayList<String> compareStringArrays(String[] regStr1, String[] regStr2) {
        ArrayList<String> resultString = new ArrayList<>();

        for (String word1 : regStr1) {
            boolean flag = false;
            for (String word2 : regStr2) {
                if ((word1.toLowerCase()).equals(word2.toLowerCase())) {
                    flag = true;
                    break;
                }
            }
            if (!flag && resultString.indexOf(word1.toLowerCase())<0) {
                resultString.add(word1.toLowerCase());
            }
        }

        return resultString;
    }

    static String getDifferences (String str1, String str2) {

        String[] regStr1 = str1.split("[\\p{Punct}\\s]+");
        String[] regStr2 = str2.split("[\\p{Punct}\\s]+");

        ArrayList<String>  resultString1 = compareStringArrays(regStr1, regStr2);
        ArrayList<String>  resultString2 = compareStringArrays(regStr2, regStr1);

        return String.join(" ", resultString1).trim() + " || " + String.join(" ", resultString2).trim();
    }

    public static void main(String[] args) {
        Scanner getStringScanner = new Scanner(System.in);

        System.out.println("Please input the first string");
        String oneString = getStringScanner.nextLine();

        System.out.println("Please input the second string");
        String anotherString = getStringScanner.nextLine();

        System.out.println("-----------------------------");
        System.out.println("The differences are here:");
        String result = getDifferences(oneString, anotherString);

        System.out.println(result);
    }
}
